﻿#include <vector>
#include <string>
#include <fstream>
#include "mnist_reader.h"
#include <cassert>
#include <exception>

typedef std::vector<float_t> vec_t;
typedef size_t label_t;


struct mnist_header {
	uint32_t magic_number;
	uint32_t num_items;
	uint32_t num_rows;
	uint32_t num_cols;
};

inline void parse_mnist_header(std::ifstream& ifs, mnist_header& header) {
	ifs.read(reinterpret_cast<char*>(&header.magic_number), 4);
	ifs.read(reinterpret_cast<char*>(&header.num_items), 4);
	ifs.read(reinterpret_cast<char*>(&header.num_rows), 4);
	ifs.read(reinterpret_cast<char*>(&header.num_cols), 4);

	if (is_little_endian()) {
		reverse_endian(&header.magic_number);
		reverse_endian(&header.num_items);
		reverse_endian(&header.num_rows);
		reverse_endian(&header.num_cols);
	}
}

inline void parse_mnist_image(void* handle,
	const mnist_header& header,
	float_t scale_min,
	float_t scale_max,
	int x_padding,
	int y_padding,
	vec_t& dst) {
	const int width = header.num_cols + 2 * x_padding;
	const int height = header.num_rows + 2 * y_padding;

	std::vector<uint8_t> image_vec(header.num_rows * header.num_cols);

	int len = 0;
	int state = read_item_data(handle, reinterpret_cast<char*>(&image_vec[0]), &len);
	if (state != 0) {
		std::string error_msg{ "reading data error"};
		std::cerr << error_msg;
		throw std::runtime_error(error_msg);
	}
	dst.resize(width * height, scale_min);

	for (uint32_t y = 0; y < header.num_rows; y++)
		for (uint32_t x = 0; x < header.num_cols; x++)
			dst[width * (y + y_padding) + x + x_padding] =
			(image_vec[y * header.num_cols + x] / float_t(255)) *
			(scale_max - scale_min) +
			scale_min;
}


inline void parse_mnist_images_asio(const std::string& image_file,
	std::vector<vec_t>* images,
	float_t scale_min,
	float_t scale_max,
	int x_padding,
	int y_padding) {
	std::ifstream ifs(image_file.c_str(), std::ios::in | std::ios::binary);

	mnist_header header;
	parse_mnist_header(ifs, header);
	ifs.close();

	const int width = header.num_cols + 2 * x_padding;
	const int height = header.num_rows + 2 * y_padding;

	images->resize(header.num_items);

	auto handle = create_item_reader(image_file.data());
	try {
		for (uint32_t i = 0; i < header.num_items; i++) {
			vec_t image;
			parse_mnist_image(handle, header, scale_min, scale_max, x_padding,
				y_padding, image);
			(*images)[i] = image;
		}
	}
	catch (std::exception e) {
		std::cerr << e.what();
	}
	
	close_item_reader(handle);

}

/**
* parse MNIST database format labels with rescaling/resizing
* http://yann.lecun.com/exdb/mnist/
*
* @param label_file [in]  filename of database (i.e.train-labels-idx1-ubyte)
* @param labels     [out] parsed label data
**/
inline void parse_mnist_labels_asio(const std::string& label_file,
	std::vector<label_t>* labels) {
	auto handle = create_item_reader(label_file.data());
	uint64_t num_items = get_item_number(handle);

	labels->resize(num_items);
	uint64_t i = 0;
	while (i < num_items) {
		uint8_t label_item[512] = { 0 };
		int len = 0;
		int state = read_item_data(handle, (char*)(&label_item[0]), &len);
		if (state != 0) {
			std::string error_msg{ "reading data error " + label_file +"\n"};
			std::cerr << error_msg;
			throw std::runtime_error(error_msg);
		}
		for (int j = 0; j < len; ++j) {
			assert(label_item[j] < 10 && label_item[j] >= 0);
			(*labels)[i++] = static_cast<label_t>(label_item[j]);
		}
	}

	close_item_reader(handle);

}
