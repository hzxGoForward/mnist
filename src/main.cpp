﻿#include <string>
#include "mnist_reader.h"
#include "mnist_parser.h"
#ifndef WIN32
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#endif
#include <iostream>


std::string read_one_item(void* handle) {
	auto new_item_num = get_item_number(handle);
	std::cout << "new item number: " << new_item_num << "\n";
	int len = 0;
	std::string buf1;
	char buf_char[1024] = { 0 };
	try {
		auto state = read_item_data(handle, buf_char, &len);
		assert(state == 0);
		buf1.resize(len, 0);
		memcpy((uint8_t*)&buf1[0], buf_char, len);
	}
	catch (std::exception& e) {
		std::cout << e.what() << std::endl;
	}
	return buf1;
}

void test_mnist_reader() {
	try
	{
#ifdef WIN32
		// t10k-images.idx3-ubyte train-labels-idx1-ubyte
		std::string file_dir = "D:\\train-labels.idx1-ubyte";
#endif
#ifndef WIN32
		std::string file_dir = "/home/zxhu/gitLab/dataset/data/t10k-images.idx3-ubyte";
#endif
		/*std::cout << "please input a path:\n";
		std::cin >> file_dir;*/

		void* handle = create_item_reader(file_dir.data());
		
		
		auto item_num = get_item_number(handle);
		std::cout << "item number: " << item_num << "\n";
		int read_len = 0;
		while(item_num--)
		{
			int len = 0;
			char buf[1024] = {0};
			read_item_data(handle, buf, &len);
			read_len += len;
			std::string ret(len, 0);
			memcpy((uint8_t*)&ret[0], buf, len);
			std::cout << "current read: " << len << ", total fetch " << read_len << " Bytes string length: "<< ret.size()<<std::endl;
			break;
		} 

		reset_for_read(handle);
		std::string buf1 = read_one_item(handle);
		std::string buf2 = read_one_item(handle);
		
		reset_for_read(handle);
		std::string buf3 = read_one_item(handle);
		std::string buf4 = read_one_item(handle);
		assert(buf1 == buf3);
		assert(buf2 == buf4);
		close_item_reader(handle);

	}
	catch (std::exception e) {
		std::cerr << "ERROR:   " << e.what() << std::endl;
	}
}

void test_mnist_parser() {
	try
	{
#ifdef WIN32
		// t10k-images.idx3-ubyte train-labels-idx1-ubyte
		std::string data_dir = "D:/";
#endif
#ifndef WIN32
		std::string data_dir = "/home/zxhu/gitLab/dataset/data/";
#endif
		std::vector<label_t> train_labels, test_labels;
		std::vector<vec_t> train_images, test_images;

		parse_mnist_labels_asio(data_dir + "/train-labels.idx1-ubyte", &train_labels);
		parse_mnist_images_asio(data_dir + "/train-images.idx3-ubyte", &train_images, -1.0, 1.0, 2, 2);
		parse_mnist_labels_asio(data_dir + "/t10k-labels.idx1-ubyte",
			&test_labels);
		parse_mnist_images_asio(data_dir + "/t10k-images.idx3-ubyte",
			&test_images, -1.0, 1.0, 2, 2);
	}
	catch (std::exception e) {
		std::cerr << "ERROR:   " << e.what() << std::endl;
	}


}

int main()
{
	test_mnist_parser();
	test_mnist_reader();

	std::cout << "test over\n";
	return 0;
}
