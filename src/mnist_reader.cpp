﻿#include "mnist_reader.h"
#include <cstring>
#include <fstream>
#include <memory>
#include <string>
#include <errno.h>
#include <fcntl.h>
#include <boost/asio.hpp>
#include <boost/asio/buffer.hpp>

static std::string file_dir = "";
static uint32_t item_num = 0;
static int64_t file_size = -1;
static int64_t buff_len = 512;
static int64_t read_len = 0;
static std::shared_ptr<asio_read> asio_read_ptr;

void initialize() {
    // initialize state
    file_dir = "";
    item_num = 0;
    file_size = -1;
    buff_len = 512;
    read_len = 0;
    asio_read_ptr = nullptr;
}

int64_t get_file_size(const char* file_name)
{
    if (file_size != -1)
        return file_size;
    std::ifstream ifs(file_name, std::ios::in | std::ios::binary);
    if (!ifs)
        throw std::invalid_argument("can not open file: " + std::string(file_name));
    ifs.seekg(0, ifs.end);
    file_size = ifs.tellg();
    ifs.close();
    return file_size;
}


void* create_item_reader(const char* file_path)
{
    initialize();

    // create item reader
    file_dir = std::string(file_path);
    auto file_size = get_file_size(file_path);
    // std::cout << file_path << " size: " << file_size << std::endl;

#ifndef WIN32
    int* fm = new int(open(file_path, O_RDONLY));
#else
    HANDLE* fm = new HANDLE(::CreateFile(
        file_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0));
    if (*fm == INVALID_HANDLE_VALUE)
    {
        throw std::invalid_argument("can not open " + std::string(file_path));
    }

#endif

    get_item_number(fm);
    return fm;
}

int reset_for_read(void* handle)
{
    if (!handle || *((int*)handle) < 0)
    {
        return -1;
    }
    // handle = create_item_reader(file_dir.data());

    if (asio_read_ptr) {
        asio_read_ptr->stop();
        asio_read_ptr = nullptr;
    }

    close_item_reader(handle);

    std::string tmp_dir = file_dir;
    auto fm = create_item_reader(tmp_dir.data());
#ifdef WIN32 
    * (HANDLE*)(handle) = *(HANDLE*)(fm);
    delete (HANDLE*)(fm);
#else
    * (int*)(handle) = *(int*)(fm);
    delete (int*)(fm);
#endif
    
//#ifdef WIN32
//    LARGE_INTEGER len = { 0 };
//    int state = SetFilePointerEx(*(HANDLE*)(handle), len, nullptr, FILE_BEGIN);
//#else
//    int state = lseek(*(int*)(handle), 0, SEEK_SET);
//#endif
//    assert(state != -1);
//    std::string tmp_file = file_dir;
//    initialize();
//    file_dir = tmp_file;
//    get_item_number(handle);
    return 0;
}

int read_item_data(void* handle, char* buf, int* len)
{
    if (!handle || !buf)
    {
        return -1;
    }
    else if (!asio_read_ptr)
    {
        // void *handle, const int64_t buff_len, const int64_t read_len, const int64_t total_read
        asio_read_ptr = std::make_shared<asio_read>(handle, buff_len, read_len, file_size);
        // asio_read_ptr->run();
    }

    CDataPkg_ptr_t read_buff_ptr = nullptr;
    bool pop_state = asio_read_ptr->pop(read_buff_ptr);
    if (pop_state && read_buff_ptr)
    {
        memcpy(buf, read_buff_ptr->data.get(), read_buff_ptr->length);
        *len = read_buff_ptr->length;
        return 0;
    }
    else
        return -1;
}

int close_item_reader(void* handle)
{
    if (asio_read_ptr)
    {
        asio_read_ptr->stop();
        asio_read_ptr = nullptr;
    }
    if (handle && *((int*)handle) < 0)
    {
#ifndef WIN32
        close(*(int*)handle);
#else
        CloseHandle(handle);
#endif
    }
    return 0;
}

uint64_t get_item_number(void* handle)
{
    if (item_num != 0)
        return item_num;
    std::ifstream ifs(file_dir.data(), std::ios::in | std::ios::binary);
    uint32_t magic_number = 0;
    ifs.read(reinterpret_cast<char*>(&magic_number), 4);
    ifs.read(reinterpret_cast<char*>(&item_num), 4);
    if (is_little_endian())
    { // MNIST data is big-endian format
        reverse_endian(&magic_number);
        reverse_endian(&item_num);
    }
    read_len += 8;
    uint32_t label = 0x00000801, image = 0x00000803;
    if (magic_number == image)
    {
        uint32_t rows = 0, cols = 0;
        ifs.read(reinterpret_cast<char*>(&rows), 4);
        ifs.read(reinterpret_cast<char*>(&cols), 4);
        if (is_little_endian())
        { // MNIST data is big-endian format
            reverse_endian(&rows);
            reverse_endian(&cols);
        }
        read_len += 8;
        buff_len = static_cast<uint64_t>(rows) * static_cast<uint64_t>(cols);
    }
    else if (magic_number != label)
    {
        ifs.close();
        throw std::logic_error("error file format " + file_dir);
    }
    ifs.close();

#ifndef WIN32
    int offset = 8;
    if (magic_number == image)
        offset = 16;
    char buf[16] = { 0 };
    read(*reinterpret_cast<int*>(handle), buf, offset);
#endif
    return static_cast<uint64_t>(item_num);
}
